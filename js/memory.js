var img = document.getElementsByTagName("img"); // On initialise un tableau contenant toutes les images
var imgReturn = document.getElementsByClassName("card");
var tdArray = document.getElementsByTagName("td");
var CardsON = [ // Tableau contenant les cartes faces active
    "img/as_noir.png",
    "img/as_noir.png",
    "img/as_rouge.png",
    "img/as_rouge.png",
    "img/roi_noir.png",
    "img/roi_noir.png",
    "img/roi_rouge.png",
    "img/roi_rouge.png",
    "img/dame_noir.png",
    "img/dame_noir.png",
    "img/dame_rouge.png",
    "img/dame_rouge.png",
    "img/valet_noir.png",
    "img/valet_noir.png",
    "img/valet_rouge.png",
    "img/valet_rouge.png"
];
var ClikedCard = [];
// Va ajouter l'image de carte retrouné
function AddCardsFace() {
    for (i = 0; i <= img.length - 1; i++) {
        img[i].src = "img/dos.png";
        img[i].id = i + 20;
    };
}
// Fonction qui va mélanger le tableau de carte afin de les placer de fâcon aléatoire
function melange(CardsOn) {
    var j = 0;
    var valI = '';
    var valJ = valI;
    var l = CardsOn.length - 1;
    while (l > -1) {
        j = Math.floor(Math.random() * l);
        valI = CardsOn[l];
        valJ = CardsOn[j];
        CardsOn[l] = valJ;
        CardsOn[j] = valI;
        l = l - 1;
    }
    return CardsOn;
}
// Va créer une nouvelle image face visibe
function setCardsOn(CardsON) {
    for (i = 0; i <= tdArray.length - 1; i++) {
        var parent = document.getElementsByTagName("td");
        parent = parent[i];
        var newImg = document.createElement("img");
        newImg.className = "card";
        newImg.src = CardsON[i];
        newImg.id = i + 40;
        parent.appendChild(newImg);
    }
}
// Fonction qui va cacher la carte de dos pour laisser place a la carte face visible
function rotateCards(count) {
    var FirstCardFace;
    var FirstCardPile;
    var FirstCardValue;
    var SecondCardFace;
    var SecondCardPile;
    var SecondCardValue;
    //console.log(CardsON); Affiche les cartes dans la console (mode triche)

    $(tdArray).on("click", function () {
        var Card = this.id;
        Card = parseInt(Card, 10);
        ClikedCard.push(Card);

        if (ClikedCard.length == 1) {

            FirstCardFace = document.getElementById(ClikedCard[0] + 20);
            FirstCardPile = document.getElementById(ClikedCard[0] + 40);
            FirstCardFace.style.display = "none";
            FirstCardPile.style.display = "block";
            FirstCardValue = FirstCardPile.src;

        } 
        if (ClikedCard.length == 2) {
            
            SecondCardFace = document.getElementById(ClikedCard[1] + 20);
            SecondCardPile = document.getElementById(ClikedCard[1] + 40);
            SecondCardFace.style.display = "none";
            SecondCardPile.style.display = "block";
            SecondCardValue = SecondCardPile.src;

            if (ClikedCard[0] != ClikedCard[1]) {
                count++;
                
                if (FirstCardValue == SecondCardValue) {
                    console.log("Voici une paire ! Félicitation !");
                    var test = document.getElementById(ClikedCard[0]);
                    var test2 = document.getElementById(ClikedCard[1]);
                    test.className = "finded";
                    test2.className = "finded";
                    ClikedCard = []
        
                } 
                else if (FirstCardValue != SecondCardValue){
                    setTimeout(function () {
                        FirstCardFace.style.display = "block";
                        FirstCardPile.style.display = "none";
                        SecondCardFace.style.display = "block";
                        SecondCardPile.style.display = "none";
                        ClikedCard = [];
                    }, 600)
                }
                
            } else {
                ClikedCard = [];
                FirstCardFace.style.display = "block";
                FirstCardPile.style.display = "none";
            }
        }
        Compteur.innerHTML = "Nombre de coup : " + count;
    })
}



// La fonction Global du Jeu, celle qui va appeller toutes les autres fonctions
function Initialise() {

    var Reset = document.createElement("button"); // Créé le bouton pour recommencer la partie
    Reset.id = "Reset";
    Reset.innerHTML = "Recommencer la partie";
    document.getElementById("Game_Info").appendChild(Reset);
    
    Reset.addEventListener("click", function() { // Le bouton recommencer recharge la page lors du clique
        void window.location.reload();
    })

    var DivCompteur = document.createElement("div") 
    DivCompteur.id = "Compteur";
    var Compteur = document.createElement("p");
    DivCompteur.appendChild(Compteur);
    Reset.after(DivCompteur);
     

    var count = 0;
    AddCardsFace(); // Initialise les cartes face caché
    melange(melange(CardsON)); // Double Melange le tableau de carte contenant les faces visibles des cartes
    setCardsOn(CardsON); // Va créer l'image face visible mais en display = none;
    rotateCards(count); // Fonction qui va gérer le retournement de carte, de vérification de valeur etc

    




}
Initialise();